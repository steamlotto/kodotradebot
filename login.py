# -*- coding: utf-8-*-

import time
import rsa
import base64
import json
import poplib
import re
import support

from grab import Grab

def login(self):

    self._flaq_session_id = False
    self._frag_go_to_login_page = False
    self._flag_take_mod_and_exp = False
    self._flag_enc_password = False
    self._flag_first_dologin = False
    self._flag_take_code = False
    self._flag_second_dologin = False
    self._flag_login_finish = False
    self.login_error = 0

    while self._flag_login_finish is False:
        try:
            self._flag_first_dologin = True
            if self._flaq_session_id is False:
                try:
                    self.go('http://steamcommunity.com/')
                    self.sessionid = re.findall('g_sessionID = "(.+?)";', self.response.body)[0]
                    self._flaq_session_id  = True
                except(Exception):
                    print 'error with load http://steamcommunity.com/'
                    time.sleep(1)
                    continue
            if self.login_error > 10:
                print 'login... self.error_counter > 10:'
                self._frag_go_to_login_page = False
                self._flag_take_mod_and_exp = False
                self._flag_enc_password = False
                self._flag_first_dologin = False
                self._flag_take_mod_and_exp = False
                self._flag_second_dologin = False
                self._flag_login_finish = False
                self.login_error = 0
            if self._flag_go_to_login_page is False:
                go_to_login_page(self)
                continue
            if self._flag_take_mod_and_exp is False:
                mod_and_exp = get_rsa_key(self)
                continue
            if self._flag_enc_password is False:
                enc_password = make_hash(self, mod_and_exp)
                continue
            if self._flag_first_dologin is False:
                do_login(self, enc_password, mod_and_exp[2])
                continue
            if self._flag_take_code is False:
                if self.code_link is None:
                    code = input_auth_mobile_code(self)
                else:
                    code = take_auth_mobile_code(self)
                continue
            if self._flag_second_dologin is False:
                do_login(self, enc_password, mod_and_exp[2], code=code)
                continue
            if self._flag_login_finish is False:
                self.go('http://steamcommunity.com/')
                if self.doc.select(".//*[(@id='headerUserAvatarIcon')]").exists() is True:
                    self._flag_login_finish = True
                else:
                    print "time.sleep. page not loading."
                    time.sleep(1)
                    self.login_error += 1
        except Exception as inst:
            continue


def go_to_login_page(self):
    try:
        self._url = "https://steamcommunity.com/login/home/?goto=0"
        self.request(url=self._url)
        self._flag_go_to_login_page = True
    except:
        print 'login. go_to_login_page. Provlem with it'
        self.error_counter += 1


def get_rsa_key(self):
    body_request = {
        'username': self.login_steam,
        'donotcache': str(int(time.time()*1000))
    }
    self.setup(url='https://steamcommunity.com/login/getrsakey/', post=body_request)
    self.request()
    str_response_body = self.response.body
    response_body = json.loads(str_response_body)
    self._flag_take_mod_and_exp = True
    return response_body["publickey_mod"], response_body['publickey_exp'],\
           response_body["timestamp"]


def make_hash(self, mod_and_exp):
    mod = long(mod_and_exp[0], 16)
    exp = long(mod_and_exp[1], 16)
    pub_key = rsa.PublicKey(mod, exp)
    crypto = rsa.encrypt(self.pass_steam, pub_key)
    enc_password = base64.b64encode(crypto)
    enc_password = enc_password.replace('+', '%2B')
    enc_password = enc_password.replace('/', '%2F')
    enc_password = enc_password.replace('=', '%3D')
    self._flag_enc_password = True
    return enc_password


def do_login(self, enc_password, timestamp, code=''):
    print code
    body_request = {
        'username': self.login_steam,
        "password": enc_password,
        "emailauth": "",
        "twofactorcode": code,
        "loginfriendlyname": "",
        "captchagid": "-1",
        "captcha_text": "",
        "emailsteamid": "",
        "rsatimestamp": timestamp,
        "remember_login": 'false',
        "donotcache": str(int(time.time()*1000))}

    body_request = support.dict_to_string(body_request)
    self.setup(url='https://steamcommunity.com/login/dologin/', post=body_request)
    self.request()
    str_response_body = self.response.body
    response_body = json.loads(str_response_body)
    print response_body

    try:
        if response_body['success'] is True:
            self.steam_id = response_body['transfer_parameters']['steamid']
            self.__frag_go_to_login_page = True
            self._flag_take_mod_and_exp = True
            self._flag_enc_password = True
            self._flag_first_dologin = True
            self._flag_take_mod_and_exp = True
            self._flag_second_dologin = True
            self._flag_login_finish = True
            self.error_counter = 0
        try:
            if response_body['emailauth_needed'] is True:
                self._flag_first_dologin = True
        except(KeyError):
            if response_body['requires_twofactor'] is True:
                self._flag_first_dologin = True
        if response_body['message'] != '':
            print response_body['message']
            print 'login =', self.login_steam
            print 'pass_steam =', self.pass_steam
            print 'unknow error. time.sleep(300)'
            time.sleep(300)
            self.error_counter = 11
    except(KeyError):
        self.error_counter += 1
        if self.error_counter > 5:
            self._flag_take_code = False
        if self.error_counter > 7:
            self._flag_take_mod_and_exp = False
            self._flag_enc_password = False
            self._flag_first_dologin = False
            self.error_counter = 0
            raw_input('all is very bad')

def login_to_mail(login_mail, pass_mail):
    try:
        server = poplib.POP3_SSL("pop.mail.ru")
        server.user(login_mail)
        server.pass_(pass_mail)
        number_last_mail = len(server.list()[1])
        mail = server.retr(number_last_mail)[1]
        server.quit()
        return mail
    except:
        print "Error with login_to_mail"


def take_code_from_mail(self):
    time.sleep(7)
    i = ""
    code = ''
    try:
        mail = login_to_mail(self.login_mail, self.pass_mail)
        for i in mail:
            if len(i) == 5:
                code = i
                break
        if len(i) != 5:
            print "is this email-code?  "+i
    except():
        print "Error with TakeCodeFromMail"
    self._flag_take_code = True
    return code

def input_auth_mobile_code(self):
    code = raw_input('Pls, input mobile code: ')
    self._flag_take_code = True
    return code


def take_auth_mobile_code(self):
    g = Grab()
    g.go(self.code_link)
    code = g.response.body
    self._flag_take_code = True
    return code


def logout(self):
    body_request = 'sessionid=%s' % self.sessionid
    self.setup(url='https://steamcommunity.com/login/logout/', post=body_request)
    self.request()
    time.sleep(3)