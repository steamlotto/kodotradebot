# -*- coding: utf-8-*-

from playhouse.postgres_ext import *

with open('database/pass.txt') as f:
    password = f.read().strip()
psql_db = PostgresqlExtDatabase(database='kodo', user='python', password=password, host='localhost', port='5432')

class BaseModel(Model):
    """A base model that will use our Postgresql database"""
    class Meta:
        database = psql_db


class Bots(BaseModel):
    id = IntegerField(primary_key=True)
    login_steam = CharField()
    pass_steam = CharField()
    login_mail = CharField()
    pass_mail = CharField()
    role = CharField()
    code_link = CharField()
    status = CharField()
    error = CharField()
    class Meta:
        db_table = "bots"


class BotOrders(BaseModel):
    id = IntegerField(primary_key=True)
    role = CharField()
    type = CharField()
    data = TextField()
    status = CharField()
    error = CharField()
    offer_link = CharField()
    offer_id = CharField()
    created_at = DateTimeField()
    updated_at = DateTimeField()
    run_at = DateTimeField()
    hash_uid = IntegerField()
    class Meta:
        db_table = "bot_orders"

class QueJobs(BaseModel):
    priority = IntegerField(primary_key=True)
    run_at = DateTimeField()
    job_id = IntegerField()
    job_class = TextField()
    args = JSONField()
    error_count = IntegerField()
    last_error = TextField()
    queue = TextField()
    class Meta:
        db_table = "que_jobs"


class Users(BaseModel):
    id = IntegerField(primary_key=True)
    nickname = CharField()
    steam_id = CharField()
    role = CharField()
    balance = DecimalField()
    payments = DecimalField()
    kodo_tickets = IntegerField()
    headshot_tickets = IntegerField()
    created_at = DateTimeField()
    updated_at = DateTimeField()
    steam_offer_link = CharField()
    class Meta:
        db_table = "users"


class Items(BaseModel):
    id = IntegerField(primary_key=True)
    bot_order_id = IntegerField()
    item_class_id = IntegerField()
    steam_id = CharField()
    class Meta:
        db_table = "items"