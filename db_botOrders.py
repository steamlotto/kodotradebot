# -*- coding: utf-8-*-

from database import *

print 'order.id, order.status, order.user_id, order.updated_at, order.type, order.error, order.offer_id, order.run_at'
for order in BotOrders.select().order_by(BotOrders.id):
    print "%s - %s - %s - %s - %s - %s - %s - %s - %s" % (order.id, order.role, order.type, order.data, order.status, order.offer_id, order.created_at, order.updated_at, order.run_at)


print '\n\nQueJobs'
for job in QueJobs.select().order_by(QueJobs.run_at):
    print "%s - %s" % (job.run_at, job.args)