# -*- coding: utf-8-*-

import json
import re
import time
import support
import grab

class SendTradeOffer():
    # send_items checkers
    error_counter = 0
    _flag_load_trade_page = False
    _flag_take_data = False
    _flag_send_items = False
    _flag_sent_confirm = False
    _flag_check_status_offers = False
    _flag_sent_finish = False


    trade_link = ''
    id_items_list = ''
    status_sent_items = None
    error = None
    offer_id = None

    #other data
    data = ''
    partner = None
    trade_offer_access_token = None

    def __init__(self):
        pass

    def append_data_in_tradeoffer(self, id_items_list, trade_link):
        self.trade_link = trade_link
        self.id_items_list = id_items_list

    def send_items(self, Bot, round):
        while self._flag_sent_finish is False and self.error_counter < 7:
            self.error_counter += 1
            if self._flag_load_trade_page is False:
                self.__go_to_trade_page(Bot)
                continue
            if self._flag_take_data is False:
                self.__take_data_offer(Bot)
                continue
            if self._flag_send_items is False:
                self.__send_items(Bot, round)
                continue
            if self._flag_sent_confirm is False:
                self.__confirm_send_item(Bot)
                continue
            self._flag_sent_finish = True
        if self.error_counter < 7:
            self.status_sent_items = 'sent'
            self.error = None
        else:
            self.status_sent_items = 'error'

    def __go_to_trade_page(self, Bot):
        try:
            Bot.go(self.trade_link)
            self.data = Bot.response.body
            if Bot.doc.select(".//*[@id='trade_yours']/div[1]/div[1]/div/img").exists() is True:
                self._flag_load_trade_page = True
            elif Bot.doc.select(".//*[@class='error_page_content']").exists() is True:
                print 'TradeOffer.__go_to_trade_page is not loaded (error message from steam)'
                self.error = 'Error message: ' + Bot.doc.select(".//*[@class='error_page_content']/div").text()
                self.error_counter = 8
            else:
                print 'trade_offer __go_to_trade_page -> check another login in this account'
                Bot.logout()
                Bot.login()

        except:
            self.error = 'trade_link page is not loaded (page not load)'
            print 'TradeOffer.__go_to_trade_page is not loaded (page not load)'

    def __take_data_offer(self, Bot):
        try:
            self.partner = re.findall("g_ulTradePartnerSteamID = '(.+?)'", self.data)[0]
            self.trade_offer_access_token = re.findall("token=(.*)", self.trade_link)[0]
            Bot.sessionid = re.findall('g_sessionID = "(.+?)";', self.data)[0]
            self._flag_take_data = True
        except:
            self.error = "Error: Bot can't __take_data_offer"
            self.error_counter = 8

    def __send_items(self, Bot, round_id):
        list_strings_items = []
        pre = 'sessionid='+Bot.sessionid+'&serverid=1&partner='+self.partner+'&tradeoffermessage=%D0%9F%D0%BE%D0%B7%D0%B4%D1%80%D0%B0%D0%B2%D0%BB%D1%8F%D0%B5%D0%BC+%D1%81+%D0%BF%D0%BE%D0%B1%D0%B5%D0%B4%D0%BE%D0%B9!+%D0%9E%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D1%8F%D0%B5%D0%BC+%D0%B2%D0%B0%D1%88+%D0%B2%D1%8B%D0%B8%D0%B3%D1%80%D1%8B%D1%88+%D0%B2+%D0%B8%D0%B3%D1%80%D0%B5+%22'+str(round_id)+'%22&json_tradeoffer=%7B%22newversion%22%3Atrue%2C%22version%22%3A2%2C%22me%22%3A%7B%22assets%22%3A%5B'
        post = '%5D%2C%22currency%22%3A%5B%5D%2C%22ready%22%3Afalse%7D%2C%22them%22%3A%7B%22assets%22%3A%5B%5D%2C%22currency%22%3A%5B%5D%2C%22ready%22%3Afalse%7D%7D&captcha=&trade_offer_create_params=%7B%22trade_offer_access_token%22%3A%22'+self.trade_offer_access_token+'%22%7D'
        for id in self.id_items_list:
            url_item = '%7b%22appid%22%3a'+Bot.appid+'%2c%22contextid%22%3a%222%22%2c%22amount%22%3a1%2c%22assetid%22%3a%22'+id.encode('utf-8')+'%22%7d'
            list_strings_items.append(url_item)
        string_items = '%2c'.join(list_strings_items)
        body_request = pre+string_items+post
        body_request = str(body_request)
        try:
            Bot.setup(url='https://steamcommunity.com/tradeoffer/new/send', log_file="__send_items.html", post=body_request)
            Bot.request()
        except(Exception):
            self.error = "can't make https://steamcommunity.com/tradeoffer/new/send"
            return None
        str_response_body = Bot.response.body
        response_body = json.loads(str_response_body)

        try:
            str_error = response_body['strError']
            print 'strError'
            print str_error
            if str_error.find("(26)") != -1:
                self.error_counter += 1
            if str_error.find("(8)") != -1:
                self._flag_load_trade_page = False
                self._flag_take_data = False
            self.error = "Send error: " + str_error
            time.sleep(4)
        except(KeyError):
            print response_body
            self.offer_id = response_body['tradeofferid']
            self._flag_send_items = True

    def __confirm_send_item(self, Bot):
        """
        time.sleep(7)
        i = ''
        try:
            mail = login.login_to_mail(Bot.login_mail, Bot.pass_mail)
            for i in mail:
                found_ls = re.findall('https://steamcommunity.com/tradeoffer/.*/confirm\?accountid.*?"', i)
                if len(found_ls) != 0:
                    Bot.go(found_ls[0][:-1])
                    break
        except():
            print "Error with confirm_trade_offer"
        """
        self._flag_sent_confirm = True


    def return_list_send_items(self, Bot):
        list_send_items = []
        i = 0
        while i < 3:
            try:
                Bot.go("https://steamcommunity.com/profiles/"+Bot.steam_id+"/tradeoffers/sent")
                if Bot.doc.select(".//*[@class='tradeoffer']").exists() is True:
                    if not Bot.doc.select(".//*[@class='tradeoffer_items primary']/div[3]/div[*]/@data-economy-item").exists() is True:
                        print 'not load items from https://steamcommunity.com/profiles/steamid/tradeoffers/sent'
                        raise Exception
                    for offer in Bot.doc.select(".//*[@class='tradeoffer']"):
                        status = offer.select("div[3]/div[2]").text()
                        print(status)
                        if status == '':
                            flaq = True
                        elif status.find(u'Обмен принят') != -1:
                            flaq = True
                        elif status.find(u'Обмен отклонен') != -1:
                            flaq = False
                        elif status.find(u'Предложение обмена отменено') != -1:
                            flaq = False
                        elif status.find(u'Ожидается подтверждение на мобильном устройстве') != -1:
                            url = Bot.code_link
                            url = url.replace("/bot/", '/wakeup/')
                            Bot.go(url)
                            flaq = False
                        else:
                            print 'TradeOffer with unkonow status????'
                            raise Exception
                        if flaq:
                            for data_their_items in Bot.doc.select(".//*[@class='tradeoffer_items primary']/div[3]/div[*]/@data-economy-item"):
                                id = data_their_items.text().split("/")
                                id = id[2]
                                list_send_items.append(id)
                break
            except Exception as inst:
                i += 1
                if i > 4:
                    Bot.status_sent_item = 'error'
                    Bot.error = "can't load list_send_item from https://steamcommunity.com/profiles/steamid/tradeoffers/sent/"
                time.sleep(1)
        return list_send_items



class InputTradeOffers():
    error_counter = 0
    _flag_load_inputtradeoffers_page = False
    _flag_take_data = False
    _flag_collect_inputtradedffer = False
    _flag_dict_stage1_to_stage2 = False
    _flag_find_price = False
    _flag_check_room_rules = False

    status_incoming_trader_offer = None
    error = None

    result_dict = {}
    offers_dict_stage1 = None
    offers_dict_stage2 = None

    def __init__(self):
        pass

    def check_incoming_offers(self, Bot):
        while self.error_counter < 9:
            print 'error_counter: ', self.error_counter
            error_counter = 0
            print self._flag_load_inputtradeoffers_page
            print self._flag_take_data
            print self._flag_collect_inputtradedffer
            print self._flag_dict_stage1_to_stage2
            print self._flag_find_price
            print self._flag_check_room_rules
            self.error_counter += 1
            if self._flag_load_inputtradeoffers_page is False:
                self.__go_to_trade_page(Bot)
                continue
            if self._flag_take_data is False:
                self.__take_data_offer(Bot)
                continue
            if self._flag_collect_inputtradedffer is False:
                self.__collect_inputtradeoffers(Bot)
                continue
            if self._flag_dict_stage1_to_stage2 is False:
                self.__stage1_to_stage2(Bot)
                continue
            if self._flag_find_price is False:
                self.__find_price(Bot)
                continue
            if self._flag_check_room_rules is False:
                self.__check_room_rules(Bot)
                continue
            break
        if self.error_counter < 9:
            self.status_incoming_trader_offer = 'pending'
            self.error = None
        else:
            print 'COUNTER PO ZVEZDE'
            self.status_incoming_trader_offer = 'error'
            if Bot.doc.select(".//*[@id='steamAccountName']").exists() is True:
                Bot.login()

    def __go_to_trade_page(self, Bot):
        Bot.setup(log_file='zvezda.html')
        flaq = False
        url = 'http://steamcommunity.com/profiles/'+Bot.steam_id+'/tradeoffers/'
        try:
            Bot.go(url)
            print Bot.doc.select(".//*[@class='btn_darkblue_white_innerfade btn_medium']").exists()

            if Bot.doc.select(".//*[@class='tradeoffer']").exists():
                self._flag_load_inputtradeoffers_page = True
                flaq = True
            if Bot.doc.select(".//*[@class='tradeoffer_welcome_ctn']").exists() is True:
                print 'No new incoming tradeoffer'
                self.offers_dict_stage1 = None
                self._flag_load_inputtradeoffers_page = True
                self._flag_collect_inputtradedffer = True
                self._flag_take_data = True
                self._flag_dict_stage1_to_stage2 = True
                self._flag_find_price = True
                self._flag_check_room_rules = True
                flaq = True
            if Bot.doc.select(".//*[@class='error_page_content']").exists() is True:
                print 'InputTradeOffer.__go_to_trade_page is not loaded (error message from steam)'
                self.error = 'Error message: ' + Bot.doc.select(".//*[@class='error_page_content']/div").text()
                self.error_counter = 9
                flaq = True
            if Bot.doc.select(".//*[@class='btn_darkblue_white_innerfade btn_medium']").exists() is True:
                print 'no login'
                Bot.logout()
                Bot.login()
                flaq = True
            if flaq is False:
                print '__go_to_trade_page - all is very bad.'
        except:
            self.error = 'InputTradeOffer page is not loaded (page not load)'
            print 'InputTradeOffer.__go_to_trade_page is not loaded (page not load)'

    def __take_data_offer(self, Bot):
        if Bot.sessionid is None:
            try:
                Bot.sessionid = re.findall('g_sessionID = "(.+?)";', Bot.response.body)[0]
                self._flag_take_data = True
            except:
                self.error = "Error: Bot can't __take_data_offer"
                self.error_counter = 9
        else:
            self._flag_take_data = True

    def __collect_inputtradeoffers(self, Bot):
        offers_dict = {}
        bot2 = Bot.clone()
        for offer in Bot.doc.select(".//*[@class='tradeoffer']"):
            items_list_in_this_offer = []
            offerid = offer.select('.//@id').text()[13:]
            if (offer.select(".//*[@class='tradeoffer_items_rule']").exists()) and\
                    (not offer.select(".//*[@class='tradeoffer_items secondary']/div[3]/div[*]/@data-economy-item")):
                try:
                    bot2.go("https://steamcommunity.com/tradeoffer/"+offerid)
                    display_style = bot2.doc.select(".//*[@class='pagecontent']/script[1]").text()
                    daysTheirEscrow = int(display_style[-2:-1])
                    if daysTheirEscrow != 0:
                        print '72 hours delay'
                        self.refuse_offer(Bot, offerid)
                        continue
                    offers_dict[offerid] = items_list_in_this_offer
                except Exception:
                    self.error_counter += 1
                for data_their_items in offer.select(".//*[@class='tradeoffer_items primary']/div[3]/div[*]/@data-economy-item"):
                    items_list_in_this_offer.append(data_their_items.text())
            elif (offer.select(".//*[@class='tradeoffer_items_rule']").exists()) and\
                    (offer.select(".//*[@class='tradeoffer_items secondary']/div[3]/div[*]/@data-economy-item")):
                self.refuse_offer(Bot, offerid)
                break
#           only for 1 offer in 1 request
            break

        self.offers_dict_stage1 = offers_dict
        if len(self.offers_dict_stage1) == 0:
            print 'No new incoming tradeoffer (2)'
            self.offers_dict_stage1 = None
            self._flag_load_inputtradeoffers_page = True
            self._flag_take_data = True
            self._flag_dict_stage1_to_stage2 = True
            self._flag_find_price = True
            self._flag_check_room_rules = True
        self._flag_collect_inputtradedffer = True

    def __stage1_to_stage2(self, Bot):
        """
        in this method we rewrite
        stage1_items_data
        {'774815443': ['730/2/3251257902/76561198069578169'], '**offer2_id***': [******]}
        to
        stage2_items_data
        {'774815443': {'sucsess': 'True', 'user1_id': '76561198069578169', 'user_items': '***json_data***', 'total_price': '**', 'error': ''},
         '**offer2_id**: {'sucsess': 'True', 'user1_id': '***', 'user_items': '***json_data***', 'total_price': '**', 'error': ''}
        ...
        }
        """
        g = grab.Grab(log_file='__stage1_to_stage2.html')
        g.setup(connect_timeout=3, log_file='log_stage1_to_stage2.html')
        result_data = {}


#       take user_id and data_item from  self.offers_dict_stage1
        for offer in self.offers_dict_stage1:
            items = {}
            stage2_data_dict = {'sucsess': None, 'user_id': None, 'user_items': None, 'total_dollar_price': None, 'total_rub_price': None, 'total_euro_price': None, 'error': None}
            item_info = {}
            i = 0

            if len(self.offers_dict_stage1[offer]) == 0:
                self._flag_load_inputtradeoffers_page = False
                self._flag_collect_inputtradedffer = False
                self.error_counter -= 2
                print "def __stage1_to_stage2(self): -> if len(self.offers_dict_stage1[offer]) == 0 "
                return None

            for item in self.offers_dict_stage1[offer]:
                data = item.split('/')
                print 'Data:'
                print data
                user_id = data.pop(3)
                item_id = data[2]
                appid_item = data[0]
                data_item = '/'.join(data)

                if (len(self.offers_dict_stage1[offer]) > 20) and (Bot.role != 'storage'):
                    print 'To many items in offer.'
                    stage2_data_dict['sucsess'] = False
                    stage2_data_dict['error'] = 'To many items in offer.'
                    break

                if appid_item != Bot.appid:
                    stage2_data_dict['sucsess'] = False
                    stage2_data_dict['error'] = 'Not %s item was found!' % Bot.appid
                    break

                while i <= 3:
                    try:
                        g.go('http://steamcommunity.com/economy/itemhover/'+data_item+'?&content_only=1&omit_owner=1&l=russian&o='+user_id)
                        s = g.response.body
                        s = s.replace('\r\n', '')
                        id = re.findall('({"id":.+).* \);', s)[0]
                        id = id.replace('<i>', '')
                        js_response = json.loads(id)
                        item_info['classid'] = js_response['classid']
                        item_info['market_hash_name'] = js_response['market_hash_name']
                        item_info['icon_url'] = js_response['icon_url']
                        item_info['name_color'] = js_response['name_color']
                        items[item_id] = item_info.copy()
                        stage2_data_dict['sucsess'] = None
                        stage2_data_dict['error'] = None
                        break
                    except:
                        print 'miss'
                        i += 1
                        stage2_data_dict['sucsess'] = False
                        stage2_data_dict['error'] = "can't take market_hash_name item from steam "
                        time.sleep(15)
            stage2_data_dict['user_id'] = user_id
            stage2_data_dict['user_items'] = items
            result_data[offer] = stage2_data_dict
        g.setup(connect_timeout=20)
        self.result_dict = result_data
        self._flag_dict_stage1_to_stage2 = True

    def __find_price(self, Bot):
        g = grab.Grab(log_file='__fund_price.html')
        currency_rate = Bot.currency_rate
        for offer in self.result_dict.values():
            if offer['sucsess'] is not False:
                total_dollar_price = 0
                total_rub_price = 0
                total_euro_price = 0
                for items_id in offer['user_items']:
                    i = 0
                    while i <= 3:
                        try:
                            market_hash_name = offer['user_items'][items_id]['market_hash_name']
                            g.go('http://steamcommunity.com/market/priceoverview/?country=RU&currency=1&appid='+Bot.appid+'&market_hash_name='+market_hash_name)
                            response = json.loads(g.response.body)
                            if response is None:
                                dollar_price = support.take_price_steamcompanion(market_hash_name, Bot.appid)
                                rub_price = str(float(dollar_price)*float(currency_rate['dollar']))
                                euro_pice = str(float(rub_price)/float(currency_rate['euro']))
                                offer['user_items'][items_id]['dollar_price'] = dollar_price
                                offer['user_items'][items_id]['rub_price'] = rub_price
                                offer['user_items'][items_id]['euro_price'] = euro_pice
                            else:
                                if response['success'] is True:
                                    try:
                                        dollar_price = response['median_price'][1:-4]
                                    except:
                                        dollar_price = response['lowest_price'][1:-4]
                                    rub_price = str(float(dollar_price)*float(currency_rate['dollar']))
                                    euro_pice = str(float(rub_price)/float(currency_rate['euro']))
                                    offer['user_items'][items_id]['dollar_price'] = dollar_price
                                    offer['user_items'][items_id]['rub_price'] = rub_price
                                    offer['user_items'][items_id]['euro_price'] = euro_pice
                            total_dollar_price += float(offer['user_items'][items_id]['dollar_price'])
                            total_rub_price += float(offer['user_items'][items_id]['rub_price'])
                            total_euro_price += float(offer['user_items'][items_id]['euro_price'])
                            offer['sucsess'] = None
                            offer['error'] = None
                            break
                        except:
                            print 'NO PRICE, SORRY =C'
                            offer['sucsess'] = False
                            offer['error'] = "Can't take item price item from steam. Please, try later."
                            i += 1
                            time.sleep(2)
                offer['total_dollar_price'] = str(total_dollar_price)
                offer['total_rub_price'] = str(round(total_rub_price, 2))
                offer['total_euro_price'] = str(round(total_euro_price, 2))
                print 'total rub price: ' + offer['total_rub_price']
        print '---'
        self._flag_find_price = True
        for offerid in self.result_dict.keys():
            print offerid
            print self.result_dict[offerid]

    def __check_room_rules(self, Bot):
        print self.result_dict
        role = Bot.role
        print role
        for offer in self.result_dict.values():
            if offer['sucsess'] is None:
                for items_id in offer['user_items']:
                    if offer['user_items'][items_id]['market_hash_name'].find('(Dragon King)') != -1:
                        offer['user_items'][items_id]['market_hash_name'] = offer['user_items'][items_id]['market_hash_name'][:6]+offer['user_items'][items_id]['market_hash_name'][9:]
                        offer['user_items'][items_id]['market_hash_name'] = offer['user_items'][items_id]['market_hash_name'].replace('(Dragon King)', 'Dragon King')
                    if offer['user_items'][items_id]['market_hash_name'].find(' StatTrak') != -1:
                        offer['user_items'][items_id]['market_hash_name'] = offer['user_items'][items_id]['market_hash_name'][2:]
                    if offer['user_items'][items_id]['market_hash_name'].find('StatTrak') != -1:
                        offer['user_items'][items_id]['market_hash_name'] = offer['user_items'][items_id]['market_hash_name'][:8]+offer['user_items'][items_id]['market_hash_name'][9:]

        if role == 'storage':
            for offerid in self.result_dict.keys():
                print 'accept offer:', offerid
                partner = self.result_dict[offerid]['user_id']
                flaq = self.accept_offer(Bot, offerid, partner)
                print 'flaq: ', flaq
                if flaq:
                    self.result_dict[offerid]['sucsess'] = True
                else:
                    self.result_dict[offerid]['sucsess'] = False


        if role == 'room1':
            for offerid in self.result_dict:
                if (self.result_dict[offerid]['sucsess'] is None) and \
                        (len(self.result_dict[offerid]['user_items']) <= 20) and \
                        (float(self.result_dict[offerid]['total_rub_price']) >= 20.0):
                    print float(self.result_dict[offerid]['total_dollar_price'])
                    print 'accept offer:', offerid
                    partner = self.result_dict[offerid]['user_id']
                    flaq = self.accept_offer(Bot, offerid, partner)
                    print 'flaq: ', flaq
                    if flaq:
                        self.result_dict[offerid]['sucsess'] = TrueF
                    else:
                        self.result_dict[offerid]['sucsess'] = False
                else:
                    print 'refuse offer: ', offerid
                    flaq = self.refuse_offer(Bot, offerid)
                    self.result_dict[offerid]['sucsess'] = False
                    self.result_dict[offerid]['error'] = 'foul rules room1'

        if role == 'room2':
            for offerid in self.result_dict:
                if (self.result_dict[offerid]['sucsess'] is None) and \
                        (len(self.result_dict[offerid]['user_items']) <= 20) and \
                        (float(self.result_dict[offerid]['total_rub_price']) >= 20.0):
                    print float(self.result_dict[offerid]['total_dollar_price'])
                    print 'accept offer:', offerid
                    partner = self.result_dict[offerid]['user_id']
                    flaq = self.accept_offer(Bot, offerid, partner)
                    print 'flaq: ', flaq
                    if flaq:
                        self.result_dict[offerid]['sucsess'] = True
                    else:
                        self.result_dict[offerid]['sucsess'] = False
                else:
                    print 'refuse offer: ', offerid
                    flaq = self.refuse_offer(Bot, offerid)
                    self.result_dict[offerid]['sucsess'] = False
                    self.result_dict[offerid]['error'] = 'foul rules room2'

        if role == 'room3':
            for offerid in self.result_dict:
                if (self.result_dict[offerid]['sucsess'] is None) and \
                        (float(self.result_dict[offerid]['total_dollar_price']) < 10.0) and \
                        (len(self.result_dict[offerid]['user_items']) <= 10):
                    print float(self.result_dict[offerid]['total_dollar_price'])
                    print 'accept offer:', offerid
                    partner = self.result_dict[offerid]['user_id']
                    flaq = self.accept_offer(Bot, offerid, partner)
                    print 'flaq: ', flaq
                    if flaq:
                        self.result_dict[offerid]['sucsess'] = True
                    else:
                        self.result_dict[offerid]['sucsess'] = False
                else:
                    print 'refuse offer: ', offerid
                    flaq = self.refuse_offer(Bot, offerid)
                    self.result_dict[offerid]['sucsess'] = False
                    self.result_dict[offerid]['error'] = 'foul rules room3'
        self._flag_check_room_rules = True

    def accept_offer(self, Bot, offerid, partner):
        try:
            Bot.setup(connect_timeout=20, log_file='def_accept_offer.html')
            url = 'https://steamcommunity.com/tradeoffer/%s' % offerid
            Bot.go(url)
            body_request = 'sessionid='+Bot.sessionid+'&serverid=1&tradeofferid='+offerid+'&partner='+partner+'&captcha='
            try:
                Bot.setup(timeout=2)
                url = "https://steamcommunity.com/tradeoffer/%s/accept" % offerid
                Bot.setup(url=url, post=body_request)
                Bot.request()
                return True
            except grab.error.GrabTimeoutError:
                Bot.setup(timeout=15)
                return True
        except Exception:
            return False

    def refuse_offer(self, Bot, offerid):
        try:
            body_request = 'sessionid='+Bot.sessionid
            url = 'https://steamcommunity.com/tradeoffer/%s/decline' % offerid
            Bot.request(url=url, post=body_request)
            return True
        except:
            return False

    def return_payments_data(self):
        return self.result_dict