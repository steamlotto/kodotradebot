# -*- coding: utf-8-*-

import json

from item import Item


class Inventory():

    #Inventory chekers
    error_counter = 0
    _flag_choose_item_finish = False
    _flag_load_inventory_page = False
    _flag_take_item_list = False
    _found_our_item = False

    inventory_status = None
    error = None

    def __init__(self):
        self.csgo_objects_itemlist = []
        self.csgo_json_itemlist = ''

    def load_inventory(self, Bot):
        self.null()
        while self._flag_choose_item_finish is False and self.error_counter < 7:
            self.error_counter += 1
            if self._flag_load_inventory_page is False:
                Inventory.__go_to_inventory_page(self, Bot)
                continue
            if self._flag_take_item_list is False:
                Inventory.__request_csgo_item_list(self, Bot)
                continue
            if len(self.csgo_objects_itemlist) != 0:
                self._flag_choose_item_finish = True
                self.inventory_status = 'done'
                break
            else:
                raw_input('csgo_item_list = 0.... is bot empty?')
        if self.error_counter >= 7:
            self.inventory_status = 'error'
            self.error = 'inventory list from steam not load'
            self._flag_choose_item_finish = False
            self._flag_load_inventory_page = False
            self. _flag_take_item_list = False
            self._found_our_item = False

    def __go_to_inventory_page(self, Bot):
        try:
            Bot.go('http://steamcommunity.com/profiles/'+Bot.steam_id+'/inventory/')
            if Bot.doc.select(".//*[@id='filter_control']").exists() is True:
                self._flag_load_inventory_page = True
                self.error_counter = 0
        except:
            print 'inventory.py. __go_to_inventory_page not success'

    def __request_csgo_item_list(self, Bot):
        json_dict = {}
        json_list = []
        dict_items = {}
        dict_item_classes = {}
        a = {}
        try:
            Bot.go('http://steamcommunity.com/profiles/'+Bot.steam_id+'/inventory/json/730/2/')
            str_response_body = Bot.response.body
            response_body = json.loads(str_response_body)
        except:
            pass

        if (response_body['success'] and (response_body['rgDescriptions'] != [])) is True:

            for item in response_body['rgInventory']:
                dict_items[response_body['rgInventory'][item]['id']] = response_body['rgInventory'][item]['classid']

            for item_classes in response_body['rgDescriptions']:
                steam_classid = response_body['rgDescriptions'][item_classes]['classid']

                a['app_id'] = response_body['rgDescriptions'][item_classes]['appid']
                a['name'] = response_body['rgDescriptions'][item_classes]['name']
                a['market_name'] = response_body['rgDescriptions'][item_classes]['market_name']
                a['market_hash_name'] = response_body['rgDescriptions'][item_classes]['market_hash_name']
                a['icon'] = response_body['rgDescriptions'][item_classes]['icon_url']

                weapontype_and_classfield = response_body['rgDescriptions'][item_classes]['type'] #тип и редкость оружия
                weapontype_and_classfield = weapontype_and_classfield.replace(u'StatTrak™, ', u'')
                weapontype_and_classfield = weapontype_and_classfield.split(', ')
                a['weapon_type'] = weapontype_and_classfield[0] # пистолет/винтовка/нож/e.t.c
                a['classfield'] = weapontype_and_classfield[1] # ширпотреб/промышленное/тайное/e.t.c
                try:
                    a['rareness'] = response_body['rgDescriptions'][item_classes]['tags'][5]['name']#качество оружия (прямо с завода/)
                except:
                    print 'rarenes not found'
                    pass
                dict_item_classes[steam_classid] = a.copy()
            json_dict['items'] = dict_items
            json_dict['item_classes'] = dict_item_classes
            json_list.append(json_dict)
            self.csgo_json_itemlist = json_list
            for item in response_body['rgInventory']:
                self.csgo_objects_itemlist.append(Item(response_body['rgInventory'][item]['id'],
                                           response_body['rgInventory'][item]['classid'],
                                           response_body['rgInventory'][item]['pos'],
                                           response_body['rgDescriptions']))
            self._flag_take_item_list = True
            self.error_counter = 0
        elif (response_body['success'] is False):
            print 'inventory is empty'
            self.error_counter += 7
        else:
            self.error_counter += 1
            print('response with items_list from steam not found')

    def return_csgo_objsects_itemlist(self, Bot):
        return self.csgo_objects_itemlist

    def return_csgo_json_itemlist(self, Bot):
        return self.csgo_json_itemlist

    def null(self):
        self.error_counter = 0
        self._flag_choose_item_finish = False
        self._flag_load_inventory_page = False
        self._flag_take_item_list = False
        self._found_our_item = False