#!/bin/bash
PIDFILE='kodotradebot.pid'
LOG='kodotradebot.log'

case $1 in
  start) 
      echo 'starting ...'
      nohup python main.py &> $LOG &
      echo $! > $PIDFILE
    ;;
  stop)
      echo 'stoping ...'
      kill `cat $PIDFILE`
      rm $PIDFILE
    ;;
  restart)
    $0 stop
    $0 start
    ;;
esac
