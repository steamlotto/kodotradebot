# -*- coding: utf-8-*-

import urllib
from grab import Grab

def dict_to_string(body_request):
    a = urllib.urlencode(body_request)
    a = a.replace('+', '')
    a = a.replace("%27", "%22")
    a = a.replace('%25', '%')
    return a


def take_price_steamcompanion(market_hash_name):
    g = Grab(log_file = 'steamcompanion.html')
    name = market_hash_name
    flaq = False

    name = name.replace(' ', '-')
    name = name.replace('(', '')
    name = name.replace(')', '')
    name = name.replace(':', '')
    name = name.replace('|', '')
    if name.find('Dragon-King') != -1:
        name = name[:6]+'-'+name[9:]
    if name.find('-StatTrak') != -1:
        name = name[1:]
        flaq = True
    if name.find('StatTrak') != -1:
        if flaq == True:
            name = name[:9]+name[10:]
        else:
            name = name[:8]+name[9:]
    url = 'https://steamcompanion.com/csgo/%s/' % name
    g.go(url)
    dollar_price = g.doc.select(".//*[@class='price values']").text()
    dollar_price = dollar_price[1:]
    print 'print dollar_price: ' + dollar_price
    return dollar_price


def take_currency_rate():
    currency_rate = {'dollar': None, 'euro': None}
    g = Grab(log_file='cbbanl.html')
    try:
        g.go('http://www.cbr.ru/')
        dollar = g.doc.select(".//*[@id='widget_exchange']/div/table/tbody/tr[2]/td[2]").text()
        dollar = dollar[5:]
        currency_rate['dollar'] = dollar.replace(',', '.')
        euro = g.doc.select(".//*[@id='widget_exchange']/div/table/tbody/tr[3]/td[2]").text()
        euro = euro[5:]
        currency_rate['euro'] = euro.replace(',', '.')
    except:
        'cbr - not work'
        return take_currency_rate()
    return currency_rate