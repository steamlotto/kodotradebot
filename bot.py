# -*- coding: utf-8-*-
from grab import Grab
from inventory import Inventory
from tradeoffer import *

import login
import logging
import datetime

logger = logging.getLogger('grab')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)


class Bot(Grab):
    # login data
    steam_id = None
    role = None
    currency_rate = None
    my_date = None
    code_link = None
    appid = None

    sessionid = None

    login_steam = None
    pass_steam = None
    login_mail = None
    pass_mail = None

    # Invemtory object
    inventory = None

    #orders status
    #--reload_inventory
    status_reload = None
    #--send_items
    status_sent_item = None
    status_check_offer = None
    offer_id = None
    #--accepted payment by items
    status_incoming_trader_offers = None

    error = None
    error_counter = 0

    # Login chekers
    login_error = 0
    _flag_go_to_login_page = False
    _flag_take_mod_and_exp = False
    _flag_enc_password = False
    _flag_first_dologin = False
    _flag_take_code = False
    _flag_second_dologin = False
    _flag_login_finish = False

    def __init__(self, login_steam, pass_steam, login_mail, pass_mail, role, currency_rate, code_link, appid):
        headers = {
            'Accept': "text/javascript, text/html, application/xml, text/xml, */*",
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0',
            'X-Prototype-Version': '1.7',
            'X-Requested-With': 'XMLHttpRequest'
        }

        Grab.__init__(self)
        self.role = role
        cookiefile = 'cookies/cookies_'+role+'.txt'
        self.setup(headers=headers, cookiefile=cookiefile, reuse_cookies=True, debug_post=True, log_file='log_'+str(self.role)+'.html')
        self.login_steam = login_steam
        self.pass_steam = pass_steam
        self.login_mail = login_mail
        self.pass_mail = pass_mail
        self.inventory = Inventory()
        self.my_date = datetime.date.today()
        self.currency_rate = currency_rate
        self.code_link = code_link
        self.appid = appid

    def login(self):
        login.login(self)

    def logout(self):
        login.logout(self)

#   --- methods with refresh_inventory
    def refresh_inventory(self):
        self.status_reload = None
        self.error = None

        self.inventory.load_inventory(self)
        self.status_reload = self.inventory.inventory_status
        self.error = self.inventory.error

    def return_csgo_objsects_itemlist(self):
        return self.inventory.return_csgo_objsects_itemlist(self)

    def return_csgo_json_itemlist(self):
        return self.inventory.return_csgo_json_itemlist(self)

#   --- methods with send_items
    def send_items(self, classid_items_list, trade_link, round_id):
        self.status_sent_item = None
        self.error = None
        self.offer_id = None
        id_items_list = []

        self.inventory.load_inventory(self)
        item_object_list = self.inventory.return_csgo_objsects_itemlist(self)
        new_offer = SendTradeOffer()
        list_send_items = new_offer.return_list_send_items(self)

     #   """
        for item_db in classid_items_list:
            for i in range(len(item_object_list)):
                if item_db == item_object_list[i].classid:
                    if not item_object_list[i].id in list_send_items:
                        id_items_list.append(item_object_list.pop(i).id)
                        break
                    else:
                        print 'next item'
                        continue
        print str(len(classid_items_list)) + ': classid_items_list'
        print str(len(id_items_list)) + " : id_items_list"
        if len(classid_items_list) != len(id_items_list):
            self.status_sent_item = 'error'
            self.error = 'Item from classid_items_list not found in inventory'
            return
        """
        for i in range(len(item_object_list)):
            id_items_list.append(item_object_list[i].id)
        """
        new_offer.append_data_in_tradeoffer(id_items_list, trade_link)
        new_offer.send_items(self, round_id)
        self.status_sent_item = new_offer.status_sent_items
        self.error = new_offer.error
        self.offer_id = new_offer.offer_id

    def check_status_offers(self, offer_id):
        if self.error_counter > 15:
            self.status_check_offer = 'error'
            self.error = "Bot could't refuse the offer"
            return None
        xpath_search = ".//*[@id='tradeofferid_" + offer_id + "']"
        try:
            self.go('https://steamcommunity.com/id/me/tradeoffers/sent/')
        except:
            self.status_check_offer = 'error'
            self.error = 'sent offer not loaded'
        if self.doc.select(xpath_search).exists() is True:
            status = self.doc.select(xpath_search + "/div[3]/div[2]").text()
            if status == '':
                try:
                    url = 'https://steamcommunity.com/tradeoffer/%s/cancel' % offer_id
                    body_request = 'sessionid=%s' % self.sessionid
                    Bot2 = self.clone()
                    Bot2.go(url, post=body_request)
                    print '---offer %s refuse' % offer_id
                    self.status_check_offer = 'ignored'
                    self.error = None
                    self.error_counter = 0
                except:
                    self.error_counter += 1
                    self.check_status_offers(offer_id)
            elif status.find(u'Обмен принят') != -1:
                print '---offer %s done' % offer_id
                self.status_check_offer = 'done'
                self.error = None
            elif status.find(u'Обмен отклонен') != -1:
                print '---offer %s rejected' % offer_id
                self.status_check_offer = 'rejected'
                self.error = None
        else:
            try:
                url = 'https://steamcommunity.com/profiles/%s/tradeoffers/sent/?history=1' % self.steam_id
                self.go(url)
            except:
                self.status_check_offer = 'error'
                self.error = 'offers history not loaded'
            if self.doc.select(xpath_search).exists() is True:
                status = self.doc.select(xpath_search + "/div[3]/div[2]").text()
                if status.find(u'Обмен принят') != -1:
                    print '---offer %s done' % offer_id
                    self.status_check_offer = 'done'
                    self.error = None
                elif status.find(u'Обмен отклонен') != -1:
                    print '---offer %s rejected' % offer_id
                    self.status_check_offer = 'rejected'
                    self.error = None
                else:
                    self.status_check_offer = 'error'
                    self.error = status
            else:
                self.status_check_offer = 'error'
                self.error = 'offer not found in steam offers history'
                print 'Bot error: offer from Bot.list_trade_offers not found in steam offers history'
        pass

#   ---methods with replenish_the_balance_with_items
    def check_input_trade_offers(self):
        print 'zachli-check_input_trade_offers'
        self.status_incoming_trader_offers = None
        self.error = None

        new_incoming = InputTradeOffers()
        new_incoming.check_incoming_offers(self)

        self.status_incoming_trader_offers = new_incoming.status_incoming_trader_offer
        self.error = new_incoming.error
        return new_incoming.return_payments_data()

if __name__ == "__main__":
    pass