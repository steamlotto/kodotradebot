## # Спецификация на ботов # ##

# Этапы обработки задачи "BotOrder::Reload" #

* Захват задачи
Статус меняется на "accepted"
* Выполнение
Статус меняется на "done"
Пост-действиюя
* Удаление всех задач с типом "Reload" и статусом "Pending"

Возможные статусы:

* pending
* accepted
* done
* error

Errors:

* "'inventory list from steam not load'" :
Универсальная ошибка на весь модуль обновления инвентаря. Может возникать в разных случаях.

# Этапы обработки задачи "BotOrders::SendItems" #

* Захват задачи
* отправка вещи, установка таймера на этой же задачи по проверки нашей "посылки"
* Проверка посылки

Возможные статусы:

* pending
* accepted
* sent
* done
* ignored
* rejected
* error

Errors:

* "no items to send" : в базе не найдены вещи, в поле "bot_order_id" которых стоит id текущей задачи.
* "trade_link page is not loaded (page not load)" : Не смогла прогрузится страница обмена. Совсем. Скорее всего лаги серверов стима.
* "Error massage: 'error massage text'" : Страница обмена в принципе прогрузилась, но сами поля обмена не найдены. Текст ошибки стима.
* "Send error: 'error massage text'" : Ошибка при непосредственно отправки запроса на отсылку вещей. Тест ошибки. Встречалась ошибка с кодом 26  попытка отправить вещь, которая находится уже не в нашем владение.
* "Error: Bot can't __take_data_offer" : Очень редкая ошибка. Проблемы с парсингом загруженной страницы и извличением необходимых данных для посылки
* "sent offer not loaded" : не прогрузилась страница https://steamcommunity.com/id/me/tradeoffers/sent/
* "offers history not loaded" : не прогрузилась страница http://steamcommunity.com/id/oveer547/tradeoffers/sent/?history=1
* "offer not found in steam offers history" : не найден оффер с указанным id на страница "отправленые предложения" и "история предложений"
* "Bot could't refuse the offer" : Стим проигнорировал команду отмены предложения обмена.