# -*- coding: utf-8-*-
from bot import Bot
from database import *

import time
import datetime
import support
import traceback
import peewee

class Main():
    errorflag = False

    def __init__(self):
        number = None

        while True:
            print 'Pls, input room number:'
            print '"0" - storage'
            print '"1" - room1'
            print '"2" - room2'
            print '"3" - room3'
            number = raw_input()
            try:
                number = int(number)
                break
            except(Exception):
                print 'Error. Wrong room number.'

        currency_rate = support.take_currency_rate()
        #        currency_rate = 0
        #       login proxy account
        if number == 0:
            db_bot_storage = Bots.select().where(Bots.role == 'storage').get()
            role = db_bot_storage.role
            login_steam = db_bot_storage.login_steam.encode('utf-8')
            pass_steam = db_bot_storage.pass_steam.encode('utf-8')
            login_mail = db_bot_storage.login_mail.encode('utf-8')
            pass_mail = db_bot_storage.pass_mail.encode('utf-8')
            code_link = db_bot_storage.code_link
            appid = '730'
            db_bot_storage.status = 'online'
            db_bot_storage.save()

        #       login room1 account
        if number == 1:
            db_bot_r1 = Bots.select().where(Bots.role == 'room1').get()
            role = db_bot_r1.role
            login_steam = db_bot_r1.login_steam.encode('utf-8')
            pass_steam = db_bot_r1.pass_steam.encode('utf-8')
            login_mail = db_bot_r1.login_mail.encode('utf-8')
            pass_mail = db_bot_r1.pass_mail.encode('utf-8')
            code_link = db_bot_r1.code_link
            appid = '570'
            db_bot_r1.status = 'online'
            db_bot_r1.save()

        #       login room2 account
        if number == 2:
            db_bot_r2 = Bots.select().where(Bots.role == 'room2').get()
            role = db_bot_r2.role
            login_steam = db_bot_r2.login_steam.encode('utf-8')
            pass_steam = db_bot_r2.pass_steam.encode('utf-8')
            login_mail = db_bot_r2.login_mail.encode('utf-8')
            pass_mail = db_bot_r2.pass_mail.encode('utf-8')
            code_link = db_bot_r2.code_link
            appid = '730'
            db_bot_r2.status = 'online'
            db_bot_r2.save()

        #       login room3 account
        if number == 3:
            db_bot_r3 = Bots.select().where(Bots.role == 'room3').get()
            role = db_bot_r3.role
            login_steam = db_bot_r3.login_steam.encode('utf-8')
            pass_steam = db_bot_r3.pass_steam.encode('utf-8')
            login_mail = db_bot_r3.login_mail.encode('utf-8')
            pass_mail = db_bot_r3.pass_mail.encode('utf-8')
            code_link = db_bot_r3.code_link
            appid = '730'
            db_bot_r3.status = 'online'
            db_bot_r3.save()

        bot = Bot(login_steam=login_steam, pass_steam=pass_steam, login_mail=login_mail, pass_mail=pass_mail, role=role,\
                  currency_rate=currency_rate, code_link=code_link, appid=appid)
        bot.login()

        while True:
            self.main_loop(bot)

    def main_loop(self, bot):


        time.sleep(1.5)
        print 'main_loop: ' + bot.role
        print round(time.clock(), 2)
        if not self.errorflag:
            try:
                self.check_new_order(bot)
            except peewee.OperationalError as inst:
                print 'peewee error'
                print traceback.format_exc()
                print type(inst)
                print inst
                raw_input()
            except Exception as inst:
                print traceback.format_exc()
                print type(inst)
                print inst
                raw_input()

    def check_new_order(self, bot):
        role = bot.role
        bot.error = None
        delta = 2
        if role == 'storage':
            delta = 2

        today = datetime.date.today()
        if bot.my_date != today:
            bot.currency_rate = support.take_currency_rate()
            bot.my_date = today

        order = self.db_cheak_output_offer(role)
        if order:
            print '%s : check_offer' % role
            self.check_offer(order, bot)
            order.update_at = datetime.datetime.now() + datetime.timedelta(seconds=delta)
            order.save()

        order = self.db_create_output_offer(role)
        if order:
            print '%s : send_items' % role
            self.send_items(order, bot)
            order.update_at = datetime.datetime.now() + datetime.timedelta(seconds=delta)
            order.save()

        order = self.db_check_input_offers(role)
        if order:
            print '%s : accept_incoming_offer' % role
            self.accept_incoming_offers(order, bot)
            order.run_at = datetime.datetime.now() + datetime.timedelta(seconds=delta)
            order.save()

        order = self.db_check_command_refresh_inventory()
        if order:
            print '%s : db_check_command_refresh_inventory' % role
            self.refresh_inventory(order, bot)
            order.run_at = datetime.datetime.now() + datetime.timedelta(seconds=delta)
            order.save()
        return True

    # --- check methods ---
    def db_cheak_output_offer(self, role):
        try:
            order = BotOrders.select().where((BotOrders.status == 'sent') &
                                             (BotOrders.role == role) &
                                             (BotOrders.type == 'BotOrder::SendItems') &
                                             (BotOrders.run_at <= datetime.datetime.now())).get()
            return order
        except:
            return None

    def db_create_output_offer(self, role):
        try:
            order = BotOrders.select().where((BotOrders.status == 'pending') &
                                             (BotOrders.role == role) &
                                             (BotOrders.type == 'BotOrder::SendItems') &
                                             (BotOrders.run_at <= datetime.datetime.now())).get()
            return order
        except:
            return None

    def db_check_input_offers(self, role):
        try:
            order = BotOrders.select().where((BotOrders.type == 'BotOrder::AcceptItems') &
                                             (BotOrders.role == role) &
                                             ((BotOrders.status == 'pending') | (BotOrders.status == 'error')) &
                                             (BotOrders.created_at <= datetime.datetime.now())).get()
            return order
        except:
            return None

    def db_check_command_refresh_inventory(self):
        try:
            order = BotOrders.select().where((BotOrders.type == 'BotOrder::Reload') &
                                             (BotOrders.status == 'pending') &
                                             (BotOrders.created_at < datetime.datetime.now())).get()
            return order
        except:
            return None

    # --- execute methods ---
    def check_offer(self, order, bot):
        offer_id = order.offer_id
        bot.check_status_offers(offer_id)
        order.status = bot.status_check_offer
        order.error = bot.error
        order.updated_at = datetime.datetime.now()
        order.save()
        args = [order.id]
        new_data = QueJobs.create(job_class='BotOrderUpdateProcessor', args=args)
        new_data.save()

    def send_items(self, order, bot):
        order.status = 'accepted'
        order.save()
        trade_link = order.offer_link
        round_id = order.hash_uid
        classid_items_list = order.data.rstrip().split(', ')
        for i in range(len(classid_items_list)):
            classid_items_list[i] = str(classid_items_list[i])
        if len(classid_items_list) == 0:
            status = 'error'
            error = 'no items to send'
            order.status = status
            order.error = error
            order.updated_at = datetime.datetime.now()
            order.save()
        else:
            bot.send_items(classid_items_list, trade_link, round_id)
            status = bot.status_sent_item
            error = bot.error
            if status == 'error':
                run_at = datetime.datetime.now()
                if error.find(u"При отправке этого предложения обмена произошла ошибка. Пожалуйста, повторите попытку позже.") != -1:
                    delta = datetime.datetime.now() - order.created_at
                    status = "pending"
                    if delta.seconds < 180:
                        print '1st try send item again. order id: %s' % order.id
                        run_at = datetime.datetime.now() + datetime.timedelta(minutes=3)
                    elif delta.seconds < 300:
                        print '2nd try send item again. order id: %s' % order.id
                        run_at = datetime.datetime.now() + datetime.timedelta(minutes=5)
                    elif delta.seconds < 600:
                        print 'REFRESH BOT!!! and 3rd try send item again. order id: %s' % order.id
                        bot = Bot(login_steam=bot.login_steam, pass_steam=bot.pass_steam,\
                              login_mail=bot.login_mail, pass_mail=bot.pass_mail, role=bot.role, currency_rate=bot.currency_rate, code_link=bot.code_link)
                        bot.login()
                        run_at = datetime.datetime.now() + datetime.timedelta(minutes=5)
                    elif delta.seconds < 1800:
                        print 'REFRESH BOT!!! and 4rd try send item again. order id: %s' % order.id
                        bot = Bot(login_steam=bot.login_steam, pass_steam=bot.pass_steam,\
                              login_mail=bot.login_mail, pass_mail=bot.pass_mail, role=bot.role, currency_rate=bot.currency_rate, code_link=bot.code_link)
                        bot.login()
                        run_at = datetime.datetime.now() + datetime.timedelta(minutes=30)
                    else:
                        print '5th try send item again. order id: %s' % order.id
                        status = "error"
            else:
                order.offer_id = bot.offer_id
                run_at = datetime.datetime.now() + datetime.timedelta(hours=2)
            order.updated_at = datetime.datetime.now()
            order.run_at = run_at
            order.status = status
            order.error = error
            order.save()

    def accept_incoming_offers(self, order, bot):
        order.status = 'accepted'
        order.save()
        print order.status
        args = bot.check_input_trade_offers()
        if args:
            new_data = QueJobs.create(job_class='NewIncomingOfferProcessor', args=args)
            new_data.save()
        order.status = bot.status_incoming_trader_offers
        order.error = bot.error
        order.run_at = datetime.datetime.now() + datetime.timedelta(seconds=10)
        order.save()

    def refresh_inventory(self, order, bot):
        bot.refresh_inventory()
        args = bot.return_csgo_json_itemlist()
        new_data = QueJobs.create(job_class='InventoryReloadProcessor', args=args, error_count=1)
        print new_data.args
        order.updated_at = datetime.datetime.now()
        order.status = bot.status_reload
        order.error = bot.error
        order.save()
        for a_order in BotOrders.select().where((BotOrders.status == 'pending') & (BotOrders.type == 'BotOrder::Reload')):
            a_order.delete_instance()
        b_order = BotOrders.create(status='pending', type='BotOrder::Reload', role=order.role,
                                   run_at=datetime.datetime.now() + datetime.timedelta(hours=1),
                                   updated_at=datetime.datetime.now())
        b_order.save()

if __name__ == "__main__":
    Main()
