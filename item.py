# -*- coding: utf-8-*-


class Item():
    id = ''
    classid = ''
    pos = ''
    market_hash_name = ''
    market_name = ''
    tradable = ''

    def __init__(self, id, classid, pos, rgDescriptions):
        self.id = id
        self.classid = classid
        self.pos = pos

        flag = False
        for rg_classid in rgDescriptions:
            if rgDescriptions[rg_classid]['classid'] == classid:
                self.market_hash_name = rgDescriptions[rg_classid]['market_hash_name']
                self.market_name = rgDescriptions[rg_classid]['market_name']
                self.tradable = rgDescriptions[rg_classid]['tradable']
                flag = True
                break
        if flag is False:
            raw_input('Item from "Task.txt" not found in inventory')

    def return_item_id(self):
        return self.id

    def return_item_classid(self):
        return self.classid

    def return_item_market_hash_name(self):
        return self.market_hash_name

    def return_item_market_name(self):
        return self.market_name

    def return_item_tradable(self):
        return self.tradable

    def return_item_pos(self):
        return self.pos